#include <stdio.h>
#include <stdbool.h>
#include <stdarg.h>
#include "scope.h"
#include "calc3.h" // !important: before include y.tab.h (othw error: nodeType*...)
#include "y.tab.h"

#include "codegen.h"
#include "common.h"

#define reg_init(index) \
    { Reg, .reg = (index) }

#define none \
    (Yield){ None_yield }

#define value(value) \
    (Yield){ Oper, .oper = (Operand) { Val, .val = (value) } }

#define reg(index) \
    (Yield){ Oper, .oper = reg_init(index) }

extern int yylineno;
static int cc;

bool semantic_error = false;

bool error(); // return sematinc_error

void none_check(const char *err_msg);

static void err(const char *fmt, ...);

void yield_identity(YieldType type);

// extract operand from yield
Operand extract_operand();
// one == tow ?
LOCAL bool same_oper(Operand *one, Operand *two);

// aux
Yield yield;
Instruction instr;
Operand none_oper = (Operand) {None_operand};

Operand acc = reg_init(acc_reg);
Operand acc2 = reg_init(acc_reg2);
Operand h1 = reg_init(hi_reg);
Operand l0 = reg_init(lo_reg);
Operand zero = reg_init(zero_reg);

LOCAL void make_instr(InstrType type, Operand op1, Operand op2, Operand op3);

LOCAL void emit_binary_customizable(nodeType *p, Operand perf_dest);

LOCAL void emit_binary(nodeType *p); // perf_dest = acc

Yield generate(nodeType *p, Operand *pref_dest) {
    int lbl1, lbl2;

    yield.type = None_yield;

    if (!p) return yield = none;

    switch (p->type) {
        case typeCon:
            return yield = value(p->con.value);
        case typeId: {
            if (!used_identifier_recursive(p->id.name)) {
                err("Undefined identifier ` %s `.", p->id.name);
                return yield = none;
            }
            return yield = reg(scope_register_index(p->id.name));
        }
        case typeOpr:
            switch (p->opr.oper) {
                case IF: {
                    generate(p->opr.op[0], &acc);
                    // acc2 = 0
                    make_instr(li, acc2, (Operand) {Val, .val = 0}, none_oper);
                    emit_instruction(&instr);

                    Operand cond = extract_operand();
                    // if cond is rvalue save it in accumulator
                    if (Val == cond.type) {
                        make_instr(li, acc, cond, none_oper);
                        emit_instruction(&instr);
                        cond = acc;
                    }

                    if (p->opr.nops > 2) {
                        // if ... else
                        // (cond) == 0; jump to else
                        lbl1 = ++cc;
                        make_instr(beq, cond, acc2, (Operand) {Val, .val = lbl1});
                        emit_instruction(&instr);

                        // if-stmt
                        generate(p->opr.op[1], &acc);
                        lbl2 = ++cc;
                        // fallthrough: jump after else
                        make_instr(jmp, (Operand) {Val, .val = lbl2}, none_oper, none_oper);
                        emit_instruction(&instr);

                        // else
                        write("L%03d:\n", lbl1);
                        generate(p->opr.op[2], &acc);

                        // end of if-else
                        write("L%03d:\n", lbl2);
                    } else {
                        // if ...
                        // (cond) == 0; jump to end
                        lbl1 = ++cc;
                        make_instr(beq, cond, acc2, (Operand) {Val, .val = lbl1});
                        emit_instruction(&instr);

                        generate(p->opr.op[1], &acc);
                        // end of if
                        write("L%03d:\n", lbl1);
                    }

                    return yield = none;
                }

                case WHILE: {
                    // loop label
                    lbl1 = ++cc;
                    write("L%03d:\n", lbl1);

                    // generate cond
                    generate(p->opr.op[0], &acc);

                    Operand cond = extract_operand();
                    // if cond is rvalue save it in accumulator
                    if (Val == cond.type) {
                        make_instr(li, acc, cond, none_oper);
                        emit_instruction(&instr);
                        cond = acc;
                    }

                    // acc2 = 0
                    make_instr(li, acc2, (Operand) {Val, .val = 0}, none_oper);
                    emit_instruction(&instr);
                    // (cond) == 0; jump to end
                    lbl2 = ++cc;
                    make_instr(beq, cond, acc2, (Operand) {Val, .val = lbl2});
                    emit_instruction(&instr);

                    // generate body
                    generate(p->opr.op[1], &acc);

                    // loop back
                    make_instr(jmp, (Operand) {Val, .val = lbl1}, none_oper, none_oper);
                    emit_instruction(&instr);

                    // end
                    write("L%03d:\n", lbl2);
                    return yield = none;
                }
                case VAR:
                case '=': {
                    // left side must be an lvalue
                    if (typeId != p->opr.op[0]->type) {
                        err("Left hand side of assignment must be lvalue.\n");
                    }
                    char const *name = p->opr.op[0]->id.name;

                    Yield toreturn;
                    Operand lhs;

                    if (VAR == p->opr.oper) {
                        declare_identifier(name);
                        // return if no initializer
                        if (NULL == p->opr.op[1]) {
                            return yield = none;
                        }
                        // generate lhs (to pass as dest register)
                        generate(p->opr.op[0], &acc);
                        toreturn = none;
                        // masquerade as '=' from now on
                        p->opr.oper = '=';
                    } else {
                        if (!used_identifier_recursive(name)) {
                            err("Undefined identifier ` %s `.", name);
                        }
                        // generate lhs (to pass as dest register and return register)
                        generate(p->opr.op[0], &acc);
                        toreturn = yield;
                    }
                    lhs = extract_operand();
                    emit_binary_customizable(p, lhs);

                    return yield = toreturn;
                }

                case BLOCK: {
                    // setup new env
                    Environment prev = env;
                    env.identifiers = NULL;
                    env.prev = &prev;
                    // execute
                    generate(p->opr.op[0], &acc);
                    // cleanup
                    destruct_environment();
                    env = prev;
                    break;
                    break;
                }
                case ';':
                    generate(p->opr.op[0], &acc);
                    return generate(p->opr.op[1], &acc);
                case '+':
                case '-':
                case '*': {
                    emit_binary_customizable(p, *pref_dest);
                    return yield;
                }
                case '/':
                case '%': {
                    char op = (char) p->opr.oper;
                    // '/' from now on
                    p->opr.oper = '/';
                    emit_binary_customizable(p, acc);
                    if (!same_oper(pref_dest, &acc)) {
                        InstrType type = ('/' == op) ? mflo : mfhi;
                        make_instr(type, *pref_dest, none_oper, none_oper);
                        emit_instruction(&instr);

                        yield.type = Oper;
                        yield.oper = *pref_dest;
                    } else {
                        yield.type = Oper;
                        yield.oper = ('/' == op) ? l0 : h1;
                    }
                    return yield;
                }

                case AND: case OR: {
                    p->opr.oper = (AND == p->opr.oper) ? '&' : '|';
                    const char op = (char)p->opr.oper;

                    // get lhs
                    generate(p->opr.op[0], pref_dest);
                    none_check("Logical operator: lhs is missing.");
                    Operand lhs = extract_operand();

                    // if rhs is not in pref_dest move,li there
                    if (Val == lhs.type) {
                        make_instr(li, *pref_dest, lhs, none_oper);
                        emit_instruction(&instr);
                        lhs = *pref_dest;
                    } else if (!same_oper(pref_dest, &lhs)) {
                        make_instr(move, *pref_dest, lhs, none_oper);
                        emit_instruction(&instr);
                        lhs = *pref_dest;
                    }

                    // AND lhs == 0 => jump
                    // OR  lhs != 0 => jump
                    InstrType shortcut = ('&' == op) ? beq : bne;

                    // acc2 = 0
                    make_instr(li, acc2, (Operand) {Val, .val = 0}, none_oper);
                    emit_instruction(&instr);
                    // (cond) == 0; jump to end
                    lbl1 = ++cc;
                    make_instr(shortcut, lhs, acc2, (Operand) {Val, .val = lbl1});
                    emit_instruction(&instr);

                    // rhs
                    generate(p->opr.op[1], pref_dest);
                    none_check("Logical operator: rhs is missing.");
                    Operand rhs = extract_operand();

                    // if rhs is not in pref_dest move,li there
                    if (Val == rhs.type) {
                        make_instr(li, *pref_dest, rhs, none_oper);
                        emit_instruction(&instr);
                        rhs = *pref_dest;
                    } else if (!same_oper(pref_dest, &rhs)) {
                        make_instr(move, *pref_dest, rhs, none_oper);
                        emit_instruction(&instr);
                        rhs = *pref_dest;
                    }

                    // end
                    write("L%03d:\n", lbl1);

                    // return last value
                    yield.type = Oper;
                    yield.oper = *pref_dest;
                    return yield;
                }

                    // case '/':
                    /*
                     * division
                     * */
            }
    }

    return none;
}

void emit_binary_customizable(nodeType *p, Operand perf_dest) {
    static char left_missing[] = "Operator '?' is missing left hand side.\n";
    static char right_missing[] = "Operator '?' is missing right hand side.\n";
    static int op_index = 10;
    // get operator as char
    const char op = (char) p->opr.oper;
    if (op != p->opr.oper) {
        err("Not 1 char operator.\n");
        exit(1);
    }
    // replace char in strings
    left_missing[op_index] = right_missing[op_index] = (char) p->opr.oper;

    // get lhs
    generate(p->opr.op[0], &acc);
    none_check(left_missing);
    Operand lhs = extract_operand();

    // for operator '='
    if ('=' == p->opr.oper) {
        // enable assign optimization
        generate(p->opr.op[1], &perf_dest);
    } else {
        generate(p->opr.op[1], &acc);
    }

    // get rhs
    none_check(right_missing);
    Operand rhs = extract_operand();

    // branch on immediate or register
    InstrType instr_type, cp_type;

    // if dest is different from lhs (ex: first = first * 3; here `dest` is same as `lhs`)
    // load lhs into dest
    if (!same_oper(&lhs, &perf_dest)) {
        // copy first value to reg
        cp_type = (Val == lhs.type) ? li : move;
        make_instr(cp_type, perf_dest, lhs, none_oper);
        emit_instruction(&instr);
        // lhs is now in perf_dest
        lhs = perf_dest;
    }

    // Resolve instruction.
    // Branch on immediate (there are instr. that do not support immediate)
    if (Reg == rhs.type) {
        instr_type = reg_instr[op];
    } else {
        instr_type = val_instr[op];

        // if there's no immediate instruction
        if (err_instr == instr_type) {
            make_instr(li, acc2, rhs, none_oper);
            emit_instruction(&instr);
            // rhs is now in acc2
            rhs = acc2;
            instr_type = reg_instr[op];
        }
    }

    // ex: first = first * 3; first = first (this one is discarded =see "avoid..."=)
    if (same_oper(&lhs, &perf_dest)) {
        switch (op) {
            case '=':
                lhs = rhs;
                rhs = none_oper;

                // avoid move $x, $x
                if (same_oper(&lhs, &perf_dest)) {
                    return;
                }
        }
    }

    if ('/' == op) {
        make_instr(instr_type, lhs, rhs, none_oper);
        emit_instruction(&instr);
        return;
    } else {
        // add second to first
        make_instr(instr_type, perf_dest, lhs, rhs);
        emit_instruction(&instr);
    }
    // update return
    yield.type = Oper;
    yield.oper = perf_dest;
}

void emit_binary(nodeType *p) {
    emit_binary_customizable(p, acc);
}

int ex(nodeType *p) {

    Yield ret = generate(p, &acc);

    if (None_yield != ret.type) {
        // discard
    }
    return 0;
}

// check if two operands are the same
bool same_oper(Operand *one, Operand *two) {
    if (NULL == one || NULL == two) {
        err("Null input. <%s>", __FUNCTION__);
        exit(1);
    }
    if (one->type != two->type || one->reg != two->reg) { // last check includes val != val
        return false;
    }
    return true;
}

// check yield.type
void yield_identity(YieldType type) {
    if (type != yield.type) {
        err("Mismatched yield: expected ` %d `; got ` %d `", type, yield.type);
        semantic_error = true;
    }
}

void make_instr(InstrType type, Operand op1, Operand op2, Operand op3) {
    instr.type = type;
    instr.op1 = op1;
    instr.op2 = op2;
    instr.op3 = op3;
}

Operand extract_operand() {
    yield_identity(Oper);
    return yield.oper;
}

void err(const char *fmt, ...) {
    va_list args;
    va_start(args, fmt);
    vprintf(fmt, args);
    va_end(args);
    printf(" Line: %d\n", yylineno);
    semantic_error = true;
}

bool error() {
    return semantic_error;
}

void none_check(const char *err_msg) {
    if (None_yield == yield.type) {
        err(err_msg);
        exit(2);
    }
}