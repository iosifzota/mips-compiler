  .text
  .globl main

main:
  li $2, 4
  li $3, 10
  add $4, $2, $3
  j exit

exit:
  li $v0, 10  # code for exit
  syscall

# reserved registers
# $0(r0), $1(at)
# exit code $2(v0)
# syscal print arg $4(a0)
