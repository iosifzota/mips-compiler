%{
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdbool.h>
#include "calc3.h"

extern int yylineno;

bool syntax_error = false;

/* prototypes */
nodeType *opr(int oper, int nops, ...);
nodeType *id(char* name);
nodeType *con(int value);
void freeNode(nodeType *p);
int ex(nodeType *p);
int yylex(void);

void yyerror(char *s);

nodeType *aux_nPtr;

%}

%union {
    int iValue;                 /* integer value */
    char* name;                /* symbol table "index" */
    nodeType *nPtr;             /* node pointer */
};

%token <iValue> INTEGER
%token <name> IDENTIFIER
%token WHILE IF PRINT BLOCK FOR VAR
%nonassoc IFX
%nonassoc ELSE

%right ASSIGN
%left OR
%left AND
%left GE LE EQ NE '>' '<'
%left '+' '-'
%left '*' '/' '%'
%nonassoc UMINUS

%type <nPtr> stmt expr stmt_list stmt_expr initializer opt_expr error decl var_decl

%%

program:
        function               ; //{ exit(0); }

function:
          function decl         { if(!syntax_error) ex($2); freeNode($2); }
        | /* NULL */
        ;
decl:
	  stmt                           { $$ = $1; }
	| var_decl                       { $$ = $1; }
	;
        /* `%prec IFX` only removes shift-reduce conflict */

var_decl:
          VAR IDENTIFIER ';'             { $$ = opr(VAR, 2, id($2), NULL); }
	| VAR IDENTIFIER '=' expr ';'    { $$ = opr(VAR, 2, id($2), $4); }
        ;

stmt:
          stmt_expr                                   { $$ = $1; }
        | PRINT expr ';'                 	      { $$ = opr(PRINT, 1, $2); }
        | WHILE '(' expr ')' stmt                     { $$ = opr(WHILE, 2, $3, $5); }
        | FOR '(' initializer opt_expr ';' opt_expr ')' stmt
              {
                /* stmt ++ postexpr */
                aux_nPtr = opr(';', 2, $8, $6); // maybe switch on BLOCK and insert stmt inside (';', 2, stmt_list, stmt)

                /* While {} */
                if (NULL == $4) {
                  /* If stmt_expr is empty; insert "true" loop condition. */
                  aux_nPtr = opr(WHILE, 2, con(1), aux_nPtr);
                }
                else {
                  aux_nPtr = opr(WHILE, 2, $4, aux_nPtr);
                }

                /* initializer ++ While {} */
                aux_nPtr = opr(';', 2, $3, aux_nPtr);
                /* { } */
                $$ = opr(BLOCK, 1, aux_nPtr);
              }
        | IF '(' expr ')' stmt %prec IFX              { $$ = opr(IF, 2, $3, $5); }
        | IF '(' expr ')' stmt ELSE stmt              { $$ = opr(IF, 3, $3, $5, $7); }
        | '{' stmt_list '}'                           { $$ = opr(BLOCK, 1, $2); }
        | error ';'
        | error '}'
        ;

stmt_list:
          decl                  { $$ = $1; }
        | stmt_list decl        { $$ = opr(';', 2, $1, $2); }
        |                       { $$ = NULL; }
        ;

  // used by for only
initializer:
          stmt_expr                      { $$ = $1; }
        | PRINT expr ';'                 { $$ = opr(PRINT, 1, $2); }
        | var_decl                       { $$ = $1; }
        ;

stmt_expr:
          ';'                            { $$ = opr(';', 2, NULL, NULL); }
        | expr ';'                       { $$ = $1; }
        ;

  // used by for only
opt_expr:
          expr                           { $$ = $1; }
        | /* nothing */                  { $$ = NULL; }
        ;

expr:
          INTEGER                            { $$ = con($1); }
        | IDENTIFIER                         { $$ = id($1); }
        | '-' expr %prec UMINUS              { $$ = opr(UMINUS, 1, $2); }
        | IDENTIFIER '=' expr %prec ASSIGN   { $$ = opr('=', 2, id($1), $3); }
        | expr '+' expr                      { $$ = opr('+', 2, $1, $3); }
        | expr '-' expr                      { $$ = opr('-', 2, $1, $3); }
        | expr '*' expr                      { $$ = opr('*', 2, $1, $3); }
        | expr '/' expr                      { $$ = opr('/', 2, $1, $3); }
        | expr '%' expr                      { $$ = opr('%', 2, $1, $3); }
        | expr '<' expr                      { $$ = opr('<', 2, $1, $3); }
        | expr '>' expr                      { $$ = opr('>', 2, $1, $3); }
        | expr GE expr                       { $$ = opr(GE, 2, $1, $3); }
        | expr LE expr                       { $$ = opr(LE, 2, $1, $3); }
        | expr NE expr                       { $$ = opr(NE, 2, $1, $3); }
        | expr EQ expr                       { $$ = opr(EQ, 2, $1, $3); }
        | expr OR expr                       { $$ = opr(OR, 2, $1, $3); }
        | expr AND expr                      { $$ = opr(AND, 2, $1, $3); }
        | '(' expr ')'                       { $$ = $2; }
        ;

%%

nodeType *con(int value) {
    nodeType *p;

    /* allocate node */
    if ((p = malloc(sizeof(nodeType))) == NULL)
        yyerror("out of memory");

    /* copy information */
    p->type = typeCon;
    p->con.value = value;

    return p;
}

nodeType *id(char* name) {
    nodeType *p;

    /* allocate node */
    if ((p = malloc(sizeof(nodeType))) == NULL)
        yyerror("out of memory");

    /* copy information */
    p->type = typeId;
    p->id.name = name;

    return p;
}

nodeType *opr(int oper, int nops, ...) {
    va_list ap;
    nodeType *p;
    int i;

    /* allocate node, extending op array */
    if ((p = malloc(sizeof(nodeType) + (nops-1) * sizeof(nodeType *))) == NULL)
        yyerror("out of memory");

    /* copy information */
    p->type = typeOpr;
    p->opr.oper = oper;
    p->opr.nops = nops;
    va_start(ap, nops);
    for (i = 0; i < nops; i++)
        p->opr.op[i] = va_arg(ap, nodeType*);
    va_end(ap);
    return p;
}

void freeNode(nodeType *p) {
    int i;

    if (!p) return;
    if (p->type == typeOpr) {
        for (i = 0; i < p->opr.nops; i++)
            freeNode(p->opr.op[i]);
    }
    else if (typeId == p->type) {
        free(p->id.name);
    }
    free (p);
}

void yyerror(char *s) {
    syntax_error = true;
    fprintf(stderr, "line %d: %s\n", yylineno, s);
}
