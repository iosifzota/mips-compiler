#!/bin/sh
# build environment for cygwin

# bison -d calc1.y    produces calc1.tab.c
# bison -y -d calc1.y produces y.tab.c (the standard yacc output)

# calc3
bison -y -d calc3.y
flex calc3.l
gcc -g -c y.tab.c lex.yy.c keyword.c scope.c registers.c main.c codegen.c
gcc y.tab.o lex.yy.o calc3a.c keyword.o scope.o registers.o main.o codegen.c -o calc3a.exe
gcc y.tab.o lex.yy.o calc3b.c keyword.o scope.o registers.o main.o codegen.c -o calc3b.exe
gcc y.tab.o lex.yy.o calc3g.c keyword.o scope.o registers.o main.o codegen.c -o calc3g.exe
