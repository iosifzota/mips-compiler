#include <stdio.h>
#include <stdbool.h>
#include "scope.h"
#include "calc3.h" // !important: before include y.tab.h (othw error: nodeType*...)
#include "y.tab.h"

#include <stdarg.h>

extern int yylineno;

bool semantic_error = false;

static void err(const char* fmt, ...);

// typedef struct Ptrs Ptrs;
// struct Ptrs {
//   void* ptr;
//   Ptrs *next, *prev;
// };
//
// Ptrs* head = NULL;
// void add_node(void* ptr);
// void free_nodes();
// bool defered_free_nodes = false;

int ex(nodeType *p) {
    if (!p) return 0;
    // if (semantic_error) return 0;
    switch(p->type) {
    case typeCon:       return p->con.value;
    case typeId: {
      if (!used_identifier_recursive(p->id.name)) {
        err("Undefined identifier ` %s `.", p->id.name);
        return 0;
      }
      return scope(p->id.name)[0];
    }
    case typeOpr:
        switch(p->opr.oper) {
        case VAR: {
          char* name = p->opr.op[0]->id.name;
          if (!used_identifier_recursive(name)) {
            declare_identifier(name);
          }
          return track(name)[0] = ex(p->opr.op[1]);
        }
        case WHILE:     while(ex(p->opr.op[0])) ex(p->opr.op[1]); return 0;
        case IF:        if (ex(p->opr.op[0]))
                            ex(p->opr.op[1]);
                        else if (p->opr.nops > 2)
                            ex(p->opr.op[2]);
                        return 0;
        case PRINT:     printf("%d\n", ex(p->opr.op[0])); return 0;
        case BLOCK:     {
          // save current environment
          Environment prev = env;
          // fresh env
          env.identifiers = NULL;
          env.prev = &prev;
          // execute
          ex(p->opr.op[0]);
          // destruct created env
          destruct_environment();
          // restore saved env
          env = prev;
          break;
          break;
        }
        case ';':       ex(p->opr.op[0]); return ex(p->opr.op[1]);
        case '=':       {
          // left side must be an lvalue
          if (typeId != p->opr.op[0]->type) {
            err("Left handside of assignment must be lvalue");
          }
          char* name = p->opr.op[0]->id.name;
          if (!used_identifier_recursive(name)) {
            err("Undefined identifier ` %s `.", name);
          }
          return track(name)[0] = ex(p->opr.op[1]);
        }
        case UMINUS:    return -ex(p->opr.op[0]);
        case '+':       return ex(p->opr.op[0]) + ex(p->opr.op[1]);
        case '-':       return ex(p->opr.op[0]) - ex(p->opr.op[1]);
        case '*':       return ex(p->opr.op[0]) * ex(p->opr.op[1]);
        case '/':       return ex(p->opr.op[0]) / ex(p->opr.op[1]);
        case '<':       return ex(p->opr.op[0]) < ex(p->opr.op[1]);
        case '>':       return ex(p->opr.op[0]) > ex(p->opr.op[1]);
        case GE:        return ex(p->opr.op[0]) >= ex(p->opr.op[1]);
        case LE:        return ex(p->opr.op[0]) <= ex(p->opr.op[1]);
        case NE:        return ex(p->opr.op[0]) != ex(p->opr.op[1]);
        case EQ:        return ex(p->opr.op[0]) == ex(p->opr.op[1]);
        case OR:
          if (ex(p->opr.op[0])) {
            return 255; // true
          }
          return ex(p->opr.op[1]);

        case AND:
          if (!ex(p->opr.op[0])) {
            return 0; // false
          }
          return ex(p->opr.op[1]);
        }
    }
    return 0;
}

void err(const char* fmt, ...) {
  va_list args;
  va_start(args, fmt);
  vprintf(fmt, args);
  va_end(args);
  printf(" Line: %d\n", yylineno);
  semantic_error = true;
}

// void add_node(void* node) {
//   Ptrs* ptr;
//   ptr = (Ptrs*) malloc(sizeof(*ptr));
//   ptr->ptr = node;
//   DL_PREPEND(head, ptr);
// }
//
// void freeNode(nodeType *p);
// void free_nodes() {
//   Ptrs *current, *tmp;
//   DL_FOREACH_SAFE(head,current,tmp) {
//     DL_DELETE(head,current);
//     freeNode((nodeType*)current->ptr);
//     free(current);
//   }
// }
